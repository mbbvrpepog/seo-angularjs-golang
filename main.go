package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
	"time"

	"github.com/gorilla/mux"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

//Set host in putintoCache

type cachePage struct {
	Url          string
	Content      string
	LastModified int64
}

var cachePages *mgo.Collection

func prerenderHandler(w http.ResponseWriter, r *http.Request) {
	requestPath := strings.TrimSuffix(r.URL.String(), "?escaped_fragment_=")
	w.Write([]byte(serveCache(requestPath)))
}

func putintoCache(url string) string {
	//Request to prerender server
	//host := "localhost"
	urln := strings.TrimSuffix(url, "?escaped_fragment_")
	requestUrl := "http://localhost:3000/http://localhost:9000" + urln
	//Inserting in db
	cpage := cachePage{}
	cpage.Url = url
	cpage.Content, cpage.LastModified = downloadPage(requestUrl)
	err := cachePages.Insert(&cpage)
	if err != nil {
		log.Fatal(err)
	}
	return cpage.Content
}
func downloadPage(requestUrl string) (string, int64) {
	resp, err := http.Get(requestUrl)
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()
	//lastmodDate := resp.Header.Get("Last-Modified")
	lastModDate := time.Now().Unix()
	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	}
	return string(b), lastModDate
}
func serveCache(url string) string {
	page := cachePage{}
	err := cachePages.Find(bson.M{"url": url}).One(&page)
	cacheDurationSeconds := time.Now().Unix() - page.LastModified
	if err != nil {
		return putintoCache(url) //if not found in db
	} else if cacheDurationSeconds > 1*60*24 { //days to set cache
		return putintoCache(url)
	} else {
		return page.Content
	}
}

func routePathHandler(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "public/index.html")
}
func hello(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, "hello")
}
func main() {
	session, err := mgo.Dial("localhost")
	if err != nil {
		log.Fatal(err)
	}
	cachePages = session.DB("cache").C("pages")
	r := mux.NewRouter()
	r.Queries("escaped_fragment_", "").HandlerFunc(prerenderHandler)
	r.PathPrefix("/public").Handler(http.StripPrefix("/public", http.FileServer(http.Dir("public/"))))
	r.Path("/hello").HandlerFunc(hello)
	r.Path("/index").HandlerFunc(routePathHandler)
	r.Path("/new").HandlerFunc(routePathHandler)
	r.Path("/index/new").HandlerFunc(routePathHandler)
	r.PathPrefix("/").Handler(http.FileServer(http.Dir("public/")))
	err = http.ListenAndServe(":9000", r)
	if err != nil {
		log.Fatal(err)
	}
	//When not using cache we can just redirect crawler
}
