use instructions
================

1. Install prerender.io source locally (on default port 3000). [notes](https://prerender.io/documentation)
2. Install mongodb & run `mongod`.
3. Build app using go & run it.

Now go to `http://localhost:9000/`

A sample angular application is running on it!

now open it in browser! then append `?escaped_fragment_` as crawlers does.

Caching will be clearly visible.